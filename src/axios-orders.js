import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://react-my-burger-5fd5c.firebaseio.com/'
})

export default instance;