import { takeEvery, all, takeLatest } from 'redux-saga/effects';

import * as actionTypes from '../actions/actionTypes';
import {
	logoutSaga,
	checkAuthTimeoutSaga,
	authUserSaga,
	authCheckStateSaga
} from './auth';
import { initIngredientsSaga } from './burgerBuilder';
import { purchaseBugerSaga, fetchOrdersSaga } from './order';

export function* watchAuth() {
	yield all([
		// all sert à lancer plusieurs tâches simultanément
		yield takeEvery(actionTypes.AUTH_CHECK_TIMEOUT, checkAuthTimeoutSaga),
		yield takeEvery(actionTypes.AUTH_INITIATE_LOGOUT, logoutSaga),
		yield takeEvery(actionTypes.AUTH_USER, authUserSaga),
		yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga)
	]);
}

export function* watchBurgerBuilder() {
	yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
}

export function* watchOrder() {
	yield takeLatest(actionTypes.PURCHASE_BURGER, purchaseBugerSaga); //takeLatest annule toute exécution en cours pour cette action et ne retient que la dernière
	yield takeEvery(actionTypes.FETCH_ORDERS, fetchOrdersSaga);
}
