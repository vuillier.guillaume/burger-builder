import React, { useState, useEffect } from 'react';

import axios from '../../axios-orders';
import Aux from '../../hoc/Auxiliary/Auxiliary';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

const BurgerBuilder = props => {
	const [purchasing, setPurchasing] = useState(false);
	const { onInitIngredients, onInitPurchase } = props;

	useEffect(() => {
		onInitIngredients();
		onInitPurchase();
	}, [onInitIngredients, onInitPurchase]);

	const updatePurchaseState = ingredients => {
		const sum = Object.keys(ingredients)
			.map(igKey => {
				return ingredients[igKey];
			})
			.reduce((curSum, el) => {
				return curSum + el;
			}, 0);

		return sum > 0;
	};

	const purchaseHandler = () => {
		if (props.isAuthenticated) {
			setPurchasing(true);
		} else {
			props.onSetAuthRedirectPath('/checkout');
			props.history.push('/auth');
		}
	};

	const purchaseCancelHandler = () => {
		setPurchasing(true);
	};

	const purchaseContinueHandler = () => {
		props.history.push('/checkout');
	};

	const disabledInfo = {
		...props.ingredients
	};

	for (let key in disabledInfo) {
		disabledInfo[key] = disabledInfo[key] <= 0;
	}
	// {salad: true, meat: false ...}

	let burger = props.error ? <p>Ingredients can't be loaded</p> : <Spinner />;
	let orderSummary = null;

	if (props.ingredients) {
		burger = (
			<Aux>
				<Burger ingredients={props.ingredients} />
				<BuildControls
					ingredientAdded={props.onAddIngredient}
					ingredientRemoved={props.onRemoveIngredient}
					disabled={disabledInfo}
					price={props.price}
					purchasable={updatePurchaseState(props.ingredients)}
					ordered={purchaseHandler}
					isAuth={props.isAuthenticated}
				/>
			</Aux>
		);

		orderSummary = (
			<OrderSummary
				ingredients={props.ingredients}
				purchaseCancelled={purchaseCancelHandler}
				purchaseContinued={purchaseContinueHandler}
				purchasePrice={props.price}
			/>
		);
	}

	return (
		<Aux>
			<Modal show={purchasing} modalClosed={purchaseCancelHandler}>
				{orderSummary}
			</Modal>
			{burger}
		</Aux>
	);
};

const mapStateToProps = state => {
	return {
		ingredients: state.burgerBuilder.ingredients,
		price: state.burgerBuilder.totalPrice,
		error: state.burgerBuilder.error,
		isAuthenticated: state.auth.token !== null
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onAddIngredient: ingName => dispatch(actions.addIngredient(ingName)),
		onRemoveIngredient: ingName => dispatch(actions.removeIngredient(ingName)),
		onInitIngredients: () => dispatch(actions.initIngredients()),
		onInitPurchase: () => dispatch(actions.purchaseInit()),
		onSetAuthRedirectPath: path => dispatch(actions.setAuthRedirectPath(path))
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(withErrorHandler(BurgerBuilder, axios));
